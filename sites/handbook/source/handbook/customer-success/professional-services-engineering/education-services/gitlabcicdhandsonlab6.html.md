---
layout: handbook-page-toc
title: "GitLab CI/CD Hands On Guide- Lab 6"
description: "This Hands On Guide Lab is designed to walk you through the lab exercises used in the GitLab CI/CD training course."
---
# GitLab CI/CD Hands On Guide- Lab 6
{:.no_toc}

## LAB 6- JOB POLICY PATTERN

1. In the GitLab Demo Cloud, locate your CICD Demo project from previous labs and open it.
2. Click on your gitlab-ci.yml file and click the **Edit** icon. 
3. In another tab, locate the [ci-structure](https://gitlab-core.us.gitlabdemo.cloud/training-sample-projects/ps-classes/gitlab-ci-cd-training/gitlab-cicd-hands-on-demo/-/snippets/1692)** code snippet and click the **Copy File Contents** icon in the upper right corner of the file.  
4. Return to your CICD Demo project and place your cursor in the last line in the body of your .gitlab-ci.yml file (under the ci-variables section) and ***paste the contents of the ci-structure code snippet***. 
5. Scroll to the top of your .gitlab-ci.yml file and add review and deploy to the stages section. 
6. Type **“Adding CI-structure Variables”** in the Commit message dialog box and click the Commit button.   
7. Validate that the configuration is valid and that the pipeline is running by hovering over the blue icon in the upper right corner of the file.
8. Click on widgets and verify your work.  

### SUGGESTIONS?

If you wish to make a change to our Hands on Guide for GitLab CI/CD- please submit your changes via Merge Request!
